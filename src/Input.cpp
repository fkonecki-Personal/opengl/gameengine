#include "headers/Input.h"

void Input::processInput(GLFWwindow* window, Camera& camera) const
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        camera.camPos += camera.camFront * camera.camSpeed * Time::GetDeltaTime();
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        camera.camPos -= camera.camFront * camera.camSpeed * Time::GetDeltaTime();
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        camera.camPos += glm::normalize(glm::cross(camera.camFront, camera.camUp)) * camera.camSpeed * Time::GetDeltaTime();
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        camera.camPos -= glm::normalize(glm::cross(camera.camFront, camera.camUp)) * camera.camSpeed * Time::GetDeltaTime();

}

void Input::processInput(GLFWwindow* window, Light& light) const
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
        light.lightPosition += glm::vec3(0, 1, 0) * Time::GetDeltaTime();
    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
        light.lightPosition += glm::vec3(0, -1, 0) * Time::GetDeltaTime();
    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
        light.lightPosition += glm::vec3(-1, 0, 0) * Time::GetDeltaTime();
    if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
        light.lightPosition += glm::vec3(1, 0, 0) * Time::GetDeltaTime();
}
