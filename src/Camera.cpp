#include "headers/Camera.h"

void Camera::MouseRotate(float deltaPitch, float deltaYaw)
{
    pitch += deltaPitch;
    yaw += deltaYaw;

    if (pitch > 89) pitch = 89;
    if (pitch < -89) pitch = -89;

    glm::vec3 direction;
    direction.x = cos(glm::radians(pitch)) * cos(glm::radians(yaw));
    direction.y = sin(glm::radians(pitch));
    direction.z = cos(glm::radians(pitch)) * sin(glm::radians(yaw));
    camFront = glm::normalize(direction);
}

void Camera::Zoom(double y)
{
    fov -= y;

    if (fov < 30) fov = 30;
    if (fov > 60) fov = 60;
}