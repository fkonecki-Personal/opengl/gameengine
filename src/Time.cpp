#include "headers/Time.h"

float Time::deltaTime = 0;
float Time::lastFrame = 0;

void Time::UpdateDeltaTime()
{
    float timeNow = static_cast<float>(glfwGetTime());
    Time::deltaTime = timeNow - Time::lastFrame;
    Time::lastFrame = timeNow;
}
