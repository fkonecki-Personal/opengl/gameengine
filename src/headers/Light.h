#pragma once 

#include <glm/glm.hpp>

class Light
{
public:

	glm::vec3 ambientIntensity;
	glm::vec3 diffuseIntensity;
	glm::vec3 specularIntensity;
	glm::vec3 lightPosition;
	glm::vec3 lightColor;

	Light(glm::vec3 _ambientIntensity, glm::vec3 _diffuseIntensity, glm::vec3 _specularIntensity,
		glm::vec3 _lightColor, glm::vec3 _lightPosition) :
		ambientIntensity(_ambientIntensity), 
		diffuseIntensity(_diffuseIntensity), 
		specularIntensity(_specularIntensity),
		lightPosition(_lightPosition),
		lightColor(_lightColor) {}

};			  