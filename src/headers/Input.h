#pragma once

#include <GLFW/glfw3.h>
#include "headers/Camera.h"
#include "headers/Light.h"

#include "headers/Time.h"

class Input
{
public:
	void processInput(GLFWwindow* window, Camera& camera) const;
	void processInput(GLFWwindow* window, Light& light) const;
};