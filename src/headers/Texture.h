#pragma once

#include <glad/glad.h>

#include "headers/stb_img.h"
#include <iostream>

class Texture
{
public:

	unsigned int diffuseMap;

	Texture(const char* path)
	{
		loadTexture(path);
	}

	unsigned int loadTexture(const char* path);
	void bindDiffuseMap();
};

