#pragma once

#include <GLFW/glfw3.h>
#include "headers/Camera.h"
#include <iostream>

class Mouse
{
public:
	double lastX = 400;
	double lastY = 300;
	float mouseSensitivity = 0.1f;

	bool mouseFirst = true;

	void mouse_callback(GLFWwindow* window, double x, double y, Camera& camera);
	void scroll_callback(GLFWwindow* window, double x, double y, Camera& camera) const;

};