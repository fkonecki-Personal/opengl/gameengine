#pragma once 

#include <GLFW/glfw3.h>

class Time
{
public:
	static float deltaTime;

	static float GetDeltaTime() { return deltaTime; }
	static void UpdateDeltaTime();

private:
	static float lastFrame;

};