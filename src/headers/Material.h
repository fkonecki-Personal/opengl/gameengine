#pragma once 

#include <glm/glm.hpp>

class Material
{
public:
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;
	float shininess;

	Material(glm::vec3 _ambient, glm::vec3 _diffuse, glm::vec3 _specular, float _shininess) : 
		ambient(_ambient), diffuse(_diffuse), specular(_specular), shininess(_shininess) {}

};

static const Material EMERALD = Material(
	glm::vec3(0.0215f, 0.1745f, 0.0215f),
	glm::vec3(0.07568f, 0.61424f, 0.07568f),
	glm::vec3(0.633f, 0.727811f, 0.633f),
	0.6f);

static const Material JADE = Material(
	glm::vec3(0.135f, 0.2225f, 0.1575f),
	glm::vec3(0.54f, 0.89f, 0.63f),
	glm::vec3(0.316228f, 0.316228f, 0.316228f),
	0.1f);

static const Material OBSIDIAN = Material(
	glm::vec3(0.05375f,	0.05f, 0.06625f),
	glm::vec3(0.18275f,	0.17f, 0.22525f),
	glm::vec3(0.332741f, 0.328634f,	0.346435f),
	0.3f);

static const Material MATERIALS[] = {EMERALD, JADE, OBSIDIAN};