#pragma once

#include <glm/glm.hpp>

class Camera
{
public:
	float camSpeed = 10.f;

	float fov = 45;
	float pitch = 0;
	float yaw = -90;

	glm::vec3 camPos = glm::vec3(0.f, 0.f, 3.f);
	glm::vec3 camFront = glm::vec3(0.f, 0.f, -1.f);
	glm::vec3 camUp = glm::vec3(0.f, 1.f, 0.f);

	void MouseRotate(float deltaPitch, float deltaYaw);
	void Zoom(double y);

};

