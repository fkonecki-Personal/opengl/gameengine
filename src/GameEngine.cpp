#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "headers/stb_img.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "headers/Shader.h"
#include "headers/Camera.h"
#include "headers/Input.h"
#include "headers/Mouse.h"
#include "headers/Time.h"
#include "headers/Data.h"
#include "headers/Light.h"
#include "headers/Material.h"
#include "headers/Texture.h"

#include <iostream>

Camera camera;
Input input;
Mouse mouse;

void framebuffer_size_callback(GLFWwindow* window, int width, int height);

void mouse_callback(GLFWwindow* window, double x, double y)
{
    mouse.mouse_callback(window, x, y, camera);
}
void scroll_callback(GLFWwindow* window, double x, double y)
{
    mouse.scroll_callback(window, x, y, camera);
}

const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

int main()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif


    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetCursorPosCallback(window, mouse_callback);

    glfwSetScrollCallback(window, scroll_callback);


    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    glEnable(GL_DEPTH_TEST);

    Texture texture = Texture("resources/container2.png");

    Shader objectShader("src/shaders/vertex.glsl", "src/shaders/fragment.glsl");
    Shader lightShader("src/shaders/light-vertex.glsl", "src/shaders/light-fragment.glsl");

    unsigned int VBO, VAO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(2);

    camera = Camera();
    input = Input();
    mouse = Mouse();
    Light light = Light(glm::vec3(0.2f, 0.2f, 0.2f), 
                        glm::vec3(0.5f, 0.5f, 0.5f), 
                        glm::vec3(1, 1, 1), 
                        cubePositions[9], 
                        glm::vec3(1, 1, 1));

    // render loop
    // ------------------------------------------------------------------------------------------------
    while (!glfwWindowShouldClose(window)) 
    {
        Time::UpdateDeltaTime();

        input.processInput(window, camera);
        input.processInput(window, light);

        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        objectShader.use();

        objectShader.setVec3("lightColor", glm::vec3(1, 1, 1));
        objectShader.setVec3("light.position", light.lightPosition);
        objectShader.setVec3("light.ambient", light.ambientIntensity);
        objectShader.setVec3("light.diffuse", light.diffuseIntensity);
        objectShader.setVec3("light.specular", light.specularIntensity);

        objectShader.setVec3("viewPos", camera.camPos);

        glm::mat4 view;
        view = glm::lookAt(camera.camPos, camera.camPos + camera.camFront, camera.camUp);

        glm::mat4 projection = glm::mat4(1.0f);
        projection = glm::perspective(glm::radians(camera.fov), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
        view = glm::translate(view, glm::vec3(0.0f, 0.0f, -3.0f));

        objectShader.setMat4("projection", projection); 
        objectShader.setMat4("view", view);

        texture.bindDiffuseMap();

        glBindVertexArray(VAO);
        objectShader.setInt("material.diffuseMap", 0);

        for (unsigned int i = 0; i < 9; i++)
        {
            glm::mat4 model = glm::mat4(1.0f);
            model = glm::translate(model, cubePositions[i]);
            float angle = 20.0f * i;
            model = glm::rotate(model, glm::radians(angle), glm::vec3(1.0f, 0.3f, 0.5f));
            objectShader.setMat4("model", model);
            objectShader.setMat4("transInvModel", glm::transpose(glm::inverse(model)));

            Material material = MATERIALS[i%3];
            objectShader.setVec3("material.specular", material.specular);
            objectShader.setFloat("material.shininess", material.shininess);

            glDrawArrays(GL_TRIANGLES, 0, 36);
        }

        
        glm::mat4 model = glm::mat4(1.0f);
        model = glm::translate(model, light.lightPosition);
        float angle = 20.0f * 9;
        model = glm::rotate(model, glm::radians(angle), glm::vec3(1.0f, 0.3f, 0.5f));
        model = glm::scale(model, glm::vec3(.1f, 0.1f, 0.1f));

        lightShader.use();
        lightShader.setInt("material.diffuseMap", 0);

        lightShader.setMat4("projection", projection);
        lightShader.setMat4("view", view);
        lightShader.setMat4("model", model);

        glDrawArrays(GL_TRIANGLES, 0, 36);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);

    glfwTerminate();
    return 0;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}
