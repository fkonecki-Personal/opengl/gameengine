#include "headers/Mouse.h"

void Mouse::mouse_callback(GLFWwindow* window, double x, double y, Camera& camera)
{
    if (mouseFirst)
    {
        lastX = x;
        lastY = y;
        mouseFirst = false;
        return;
    }

    double xOffset = x - lastX;
    double yOffset = lastY - y;
    lastX = x;
    lastY = y;

    camera.MouseRotate(yOffset * mouseSensitivity, xOffset * mouseSensitivity);

}

void Mouse::scroll_callback(GLFWwindow* window, double x, double y, Camera& camera) const
{
    camera.Zoom(y);
}